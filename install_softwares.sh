#!/bin/sh

#1 Installing Dependency 
Install_Dependency() {
sudo apt-get install -y ca-certificates curl gnupg lsb-release apt-transport-https
}

#2 Installation of VLC Media Player
Install_VLC() {
sudo apt-get install -y vlc
}

#3 Installation of Conky and Conky-all
Install_Conky() {
sudo apt-get install -y conky conky-all
}

#4 Installation of gparted
Install_Gparted() {
sudo apt-get install -y gparted
}

#5 Installation of ubuntu_restricted_extras
Install_ubuntu_restricted_extras() {
sudo apt-get install -y ubuntu-restricted-extras
}

#6 Installation of Preload
Install_Preload() {
sudo apt-get install -y preload
}

#7 Installation of TLP for Battery Performance
Install_TLP() {
sudo apt-get install -y tlp tlp-rdw
}

#8 Installation of Timeshift
Install_timeshift() {
sudo apt-get install -y timeshift
}

#9 Installation of gnome-tweaks
Install_gnome_tweaks() {
sudo apt-get install -y gnome-tweaks
}

#10 Installation of ufw and gufw for firewall
Install_UFW() {
sudo apt-get install -y ufw gufw
sudo ufw enable
sudo ufw default deny incoming
sudo ufw default allow outgoing
}

#11 Installation of htop
Install_Htop() {
sudo apt-get install -y htop
}

#12 Installation of synaptic for snaps
Install_Synaptic() {
sudo apt-get install -y synaptic gdebi
}

#13 Installing youtube dl
Install_Youtube_dl() {
sudo apt-get install -y youtube-dl
}

#14 Installing mlocate
Install_Mlocate() {
sudo apt-get install -y mlocate
sudo updatedb
}

#15 installing wine wine64 and winetricks
Install_wine() {
sudo apt-get install -y wine wine64 winetricks git pipenv pipx
}

#16 installing OBS studio
Install_OBS_Studio() {
sudo apt-get install -y obs-studio
}

#17 Installing Photoshop
Install_Photoshop() {
git clone https://github.com/Gictorbit/photoshopCClinux.git
cd photoshopCClinux
chmod +x setup.sh
./setup.sh
}

#18 Installing Brave Browser
Install_Brave(){
sudo curl -fsSLo /usr/share/keyrings/brave-browser-archive-keyring.gpg https://brave-browser-apt-release.s3.brave.com/brave-browser-archive-keyring.gpg
echo "deb [signed-by=/usr/share/keyrings/brave-browser-archive-keyring.gpg arch=amd64] https://brave-browser-apt-release.s3.brave.com/ stable main"|sudo tee /etc/apt/sources.list.d/brave-browser-release.list
sudo apt-get update
sudo apt-get install -y brave-browser
}

#19 Installing Visual Studio Code
Install_Visual_Studio_Code() {
sudo apt install software-properties-common apt-transport-https wget
wget -q https://packages.microsoft.com/keys/microsoft.asc -O- | sudo apt-key add -
sudo add-apt-repository "deb [arch=amd64] https://packages.microsoft.com/repos/vscode stable main"
sudo apt-get install -y code
}

#20 Installing Okular
Install_Okular() {
sudo apt-get install -y okular
}

#21 Installing Stacer
Install_Stacer() {
sudo apt-get install -y stacer
}


#22 Installing virtualbox
Install_Virtualbox() {
sudo apt-get install -y virtualbox
}

#23 Installing ClamAV
Install_ClamAV() {
sudo apt-get install -y clamav clamtk 
}

#update the operating system
sudo apt update; sudo apt upgrade

Install_Dependency
Install_ubuntu_restricted_extras
Install_wine
Install_VLC
Install_Conky
Install_Gparted
Install_Preload
Install_TLP
Install_timeshift
Install_gnome_tweaks
Install_UFW
Install_Htop
Install_Synaptic
Install_Stacer
Install_Youtube_dl
Install_Mlocate
Install_OBS_Studio
Install_Okular
Install_Visual_Studio_Code
Install_Virtualbox
Install_ClamAV
Install_Brave
Install_Photoshop

